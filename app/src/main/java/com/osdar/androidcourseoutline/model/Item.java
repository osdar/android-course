package com.osdar.androidcourseoutline.model;

/**
 * Created by Osama on 4/17/2017 --- 11:59 PM.
 */

public class Item {

    private String productName;
    private String productPrice;
    private String productImage;

    public Item(String productName, String productPrice, String productImage) {
        this.productName = productName;
        this.productPrice = productPrice;
        this.productImage = productImage;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    @Override
    public String toString() {
        return "Item{" +
                "productName='" + productName + '\'' +
                '}';
    }
}
