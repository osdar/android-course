package com.osdar.androidcourseoutline.uiPackage;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;

import com.osdar.androidcourseoutline.R;
import com.osdar.androidcourseoutline.adapters.RecyclerViewAdapter;
import com.osdar.androidcourseoutline.model.Item;

import java.util.ArrayList;
import java.util.List;

public class ContainersActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private TabHost mTabHost;
    private ListView mListView;
    private SwipeRefreshLayout mSwipeRefresh;
    private RecyclerView mRecyclerView;
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_containers);
        initView();
        List<Item> items = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            items.add(new Item("Milk", "15.3", "https://image.freepik.com/free-vector/milk-bottle_1020-433.jpg"));
        }
        /**
         * List view
         */
        ArrayAdapter<Item> stringArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, items);
        mListView.setAdapter(stringArrayAdapter);
        /**
         * RecyclerView
         */
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        RecyclerView.LayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        // set adapter
        RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(items, this);
        mRecyclerView.setAdapter(recyclerViewAdapter);

        /**
         * Swipe Refresh Layout
         */
        mSwipeRefresh.setOnRefreshListener(this);

    }

    private void initView() {
        mListView = (ListView) findViewById(R.id.list_view);
        mSwipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mTabHost = (TabHost) findViewById(R.id.tabHost);
        mTabHost.setup();

        //Tab 1
        TabHost.TabSpec spec = mTabHost.newTabSpec("Tab One");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Tab One");
        mTabHost.addTab(spec);

        //Tab 2
        spec = mTabHost.newTabSpec("Tab Two");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Tab Two");
        mTabHost.addTab(spec);

    }

    @Override
    public void onRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefresh.setRefreshing(false);
            }
        }, 2000);
    }
}
