package com.osdar.androidcourseoutline.uiPackage;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.osdar.androidcourseoutline.R;
import com.osdar.androidcourseoutline.uiPackage.fragments.BlankFragment1;
import com.osdar.androidcourseoutline.uiPackage.fragments.BlankFragment2;


public class ButtonNavigationActivity extends AppCompatActivity {


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new BlankFragment1();
                    break;
                case R.id.navigation_dashboard:
                    fragment = new BlankFragment2();
                    break;
                case R.id.navigation_notifications:
                    fragment = new BlankFragment1();
                    break;
                case R.id.navigation_notifications2:
                    fragment = new BlankFragment2();
                    break;
            }
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content, fragment).commit();
            return true;
        }

    };
    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FrameLayout contentFrame;
    private BottomNavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button_navigation);
        fragmentManager = getSupportFragmentManager();
        contentFrame = (FrameLayout) findViewById(R.id.content);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        fragment = new BlankFragment1();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content, fragment).commit();
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }

}
