package com.osdar.androidcourseoutline.uiPackage;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.osdar.androidcourseoutline.R;

public class BasicComponentsActivity extends AppCompatActivity {

    private EditText mUserName;
    private EditText mUserPass;
    private Button mBtnLogin;
    private LinearLayout mLinearLayout;
    private TextView mLoginTextView;
    private CheckBox mCheckBox;
    private RadioGroup mRadioGroup;
    private RadioButton mRadioButtonMale;
    private RadioButton mRadioButtonFemale;
    private Spinner mSpinner;
    private Switch mSwitch1;
    private RatingBar mRatingBar;
    private SeekBar mSeekBar;
    private TextView mSeekbarCount;
    private CheckedTextView mCheckedTextView;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_components);
        initView();
        /**
         * 1- Button
         * 2- EditText
         * 3- TextView
         */
        // in anonymous  class
        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login();
            }
        });
        // in implements interface 
        // mBtnLogin.setOnClickListener(this);
        /**
         * Check Box
         */
        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(BasicComponentsActivity.this, "Is Checked:" + isChecked +
                        " ,from code " + buttonView.isChecked(), Toast.LENGTH_LONG).show();
            }
        });

        /**
         * RadioGroup and Radio button
         */
        mRadioButtonMale.setChecked(true);
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.radioButton_male:
                        Toast.makeText(BasicComponentsActivity.this, "Male", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton_female:
                        Toast.makeText(BasicComponentsActivity.this, "female", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        /**
         * Spinner
         */
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(BasicComponentsActivity.this, "The City :" + mSpinner.getItemAtPosition(position)
                                + "\n:: Selected Item:" + mSpinner.getSelectedItem()
                                + "\n:: position:" + mSpinner.getSelectedItemPosition()
                        , Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // set data dynamically by adapter
//        List<String> countryList = new ArrayList<>();
//        for (int i = 0; i < 5; i++) {
//            countryList.add("County num " + i);
//        }
//        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, countryList);
//        mSpinner.setAdapter(spinnerAdapter);

        /**
         * switch
         */
        mSwitch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(BasicComponentsActivity.this, "Checked:" + isChecked, Toast.LENGTH_SHORT).show();
            }
        });

        /**
         * Rating bar
         */

        mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Toast.makeText(BasicComponentsActivity.this, "Rating :" + rating, Toast.LENGTH_SHORT).show();
            }
        });

        /**
         * seekbar
         */

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mSeekbarCount.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(BasicComponentsActivity.this, "seekBar:" + seekBar.getProgress(), Toast.LENGTH_SHORT).show();
            }
        });

        /**
         * checkable text view
         */

        mCheckedTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCheckedTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String value;
                        if (mCheckedTextView.isChecked()) {
                            // set cheek mark drawable and set checked property to false
                            value = "un-Checked";
                            mCheckedTextView.setCheckMarkDrawable(R.drawable.ic_radio_button_unchecked_black_24dp);
                            mCheckedTextView.setChecked(false);
                        } else {
                            // set cheek mark drawable and set checked property to true
                            value = "Checked";
                            mCheckedTextView.setCheckMarkDrawable(R.drawable.ic_check_circle_black_24dp);
                            mCheckedTextView.setChecked(true);
                        }
                        Toast.makeText(v.getContext(), value, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


        /**
         * progress bar
         */

    }


    /**
     * login action by user name and password
     * ex:
     * username == osama
     * password == osdar
     */
    private void Login() {
        String userName = mUserName.getText().toString();
        String password = mUserPass.getText().toString();
        if (!userName.equalsIgnoreCase("osama") && !password.equalsIgnoreCase("osdar")) {
            Toast.makeText(this, "Oops, error in user name or password!", Toast.LENGTH_SHORT).show();
            mUserName.setError("Error in user name!");
            mUserPass.setError("Error in password!");
            mLoginTextView.setText("Oops, error in user name or password!");
            mLoginTextView.setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
            Snackbar.make(mLinearLayout, "Oops, error in user name or password!", Snackbar.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "login successfully", Toast.LENGTH_SHORT).show();
            mLoginTextView.setText("login successfully");
            mLoginTextView.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            Snackbar.make(mLinearLayout, "login successfully", Snackbar.LENGTH_LONG).setAction("relogin",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(BasicComponentsActivity.this, "relogin", Toast.LENGTH_SHORT).show();
                        }
                    }).show();

        }
    }

    private void initView() {
        mUserName = (EditText) findViewById(R.id.user_name);
        mUserPass = (EditText) findViewById(R.id.user_pass);
        mBtnLogin = (Button) findViewById(R.id.btn_login);
        mLinearLayout = (LinearLayout) findViewById(R.id.linear_layout);
        mLoginTextView = (TextView) findViewById(R.id.login_text_view);
        mCheckBox = (CheckBox) findViewById(R.id.checkBox);
        mRadioGroup = (RadioGroup) findViewById(R.id.radio_group);
        mRadioButtonMale = (RadioButton) findViewById(R.id.radioButton_male);
        mRadioButtonFemale = (RadioButton) findViewById(R.id.radioButton_female);
        mSpinner = (Spinner) findViewById(R.id.spinner);
        mSwitch1 = (Switch) findViewById(R.id.switch1);
        mRatingBar = (RatingBar) findViewById(R.id.ratingBar);
        mSeekBar = (SeekBar) findViewById(R.id.seekBar);
        mSeekbarCount = (TextView) findViewById(R.id.seekbar_count);
        mCheckedTextView = (CheckedTextView) findViewById(R.id.checkedTextView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
    }
}
