package com.osdar.androidcourseoutline.uiPackage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.osdar.androidcourseoutline.R;

public class LayoutsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.linear_layout);
//        setContentView(R.layout.relative_layout);
//        setContentView(R.layout.frame_layout);
//        setContentView(R.layout.grid_layout);
//        setContentView(R.layout.scroll_layout);
//        setContentView(R.layout.constraint_layout);

    }
}
