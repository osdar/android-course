package com.osdar.androidcourseoutline.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.osdar.androidcourseoutline.R;
import com.osdar.androidcourseoutline.model.Item;

import java.util.List;

/**
 * Created by Osama on 4/17/2017 --- 11:52 PM.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.VH> {
    private List<Item> items;
    private Context context;


    public RecyclerViewAdapter(List<Item> items, Context context) {
        this.items = items;
        this.context = context;
    }


    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_layout, null);
        return new VH(view);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.mProductName.setText(items.get(position).getProductName());
        holder.mProductPrice.setText(items.get(position).getProductPrice());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private void initView() {

    }


    class VH extends RecyclerView.ViewHolder {
        private ImageView mProductImage;
        private TextView mProductName;
        private TextView mProductPrice;

        public VH(View convertView) {
            super(convertView);
            mProductImage = (ImageView) convertView.findViewById(R.id.product_image);
            mProductName = (TextView) convertView.findViewById(R.id.product_name);
            mProductPrice = (TextView) convertView.findViewById(R.id.product_price);
        }
    }
}
