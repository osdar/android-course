package com.osdar.androidcourseoutline.utilities;

import android.app.Activity;
import android.view.View;

/**
 * Created by Osama on 4/14/2017 --- 11:48 PM.
 */

public class UiUtils {

    public static <T extends View> T findView(View root, int id) {
        return (T) root.findViewById(id);
    }

    public static <T extends View> T findView(Activity activity, int id) {
        return (T) activity.getWindow().getDecorView().getRootView().findViewById(id);
    }
}
