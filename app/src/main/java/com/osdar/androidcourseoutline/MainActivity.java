package com.osdar.androidcourseoutline;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.osdar.androidcourseoutline.uiPackage.BasicComponentsActivity;
import com.osdar.androidcourseoutline.uiPackage.ButtonNavigationActivity;
import com.osdar.androidcourseoutline.uiPackage.ContainersActivity;
import com.osdar.androidcourseoutline.uiPackage.LayoutsActivity;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    private Button mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    private void assetsFileExample() {
//        ImageView imageView = UiUtils.findView(this, R.id.imageView);

        AssetManager assetManager = getAssets();
        InputStream inputStream = null;
        try {
            inputStream = assetManager.open("images/osdar.jpg");
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
//            imageView.setImageBitmap(bitmap);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public void basic(View view) {
        Intent intent = null;
        switch (view.getId()) {

            case R.id.layout_manager:
                intent = new Intent(view.getContext(), LayoutsActivity.class);
                break;

            case R.id.basic_components:
                intent = new Intent(view.getContext(), BasicComponentsActivity.class);
                break;
            case R.id.containers:
                intent = new Intent(view.getContext(), ContainersActivity.class);
                break;

            case R.id.button_navigation:
                intent = new Intent(view.getContext(), ButtonNavigationActivity.class);
                break;

        }
        startActivity(intent);
    }
}
